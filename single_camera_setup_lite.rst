.. _single_camera_setup:

Guidelines for a single camera setup, or 'lite' setup
=====================================================

This is the reference documentation for volunteers who decided to have a simpler recording setup for smaller or mobile events. Think of this setup for "meeting people where they are", rather than as part of a formal event. However, simpler is not "better"; a more lean setup means more work during and after the event, so consider this option carefully. 

** This page is in draft and the setup is being tested (January 2023).

If you plan to organise a conference — be it in-person or online — and intend to record it, the first thing you should do is :ref:`contact us <contact the video team>`!
If we can, we'll be happy to help you. Please see more about working with the :ref:`working_with_videoteam <video team>`, and consult the :ref:`room_setup <room setup page>`.

If you think something should be included here, don’t hesitate to :ref:`contact us` or to make a merge request on the `git repository for this documentation`__.

If you run into any problems or have questions about recording your presentation, feel free to ask us in the :code:`#debconf-video` IRC channel on OFTC and we will try help you.

__ https://salsa.debian.org/debconf-video-team/docs

Audio
--------------------

The most important thing to consider is good audio. Nowadays it is easy to record any event with a phone, but the ambient noise makes it hard to hear what a person is saying. 

The recommended hardware currently is the Rode Wireless GO II, A Dual Channel Wireless Microphone System - https://rode.com/en/microphones/wireless/wirelessgoii

This setup does not have dedicated ambient/background room noise pick up, and questions need to be repeated by the speaker to be heard, before answering them.

Full audio setup for a formal conference is described on the :ref:`hardware section <audio hardware>` page. 



Video 
--------------------

This could be as simple as a phone, but a dedicated video camcorder may be more appropriate. 
Ideally, the setup should work with gstreamer - https://gstreamer.freedesktop.org/documentation/

The recommended video recorder currently is the SEREE Video Camera Camcorder - https://www.amazon.com/Camcorder-Vlogging-Recorder-Camcorders-Batteries/dp/B07Z3BKFQW/

Support infrastructure
--------------------

Check that you have all the necessary cables by testing the intended setup beforehand.
A 4-port USB3.0 hub may be useful to charge things between talks, and connect several things to a laptop at one time. 
A tripod to keep the camera stable is useful.


Laptop output capture and live mixing is not possible with this setup
----------------------------

This simple setup does not directly integrate the speaker's laptop output. It is also recommended to keep the single camera static, and focus on the speaker only.

This setup does not allow for live mixing. Editing the footage after the event can splice in presenter tools, additional footage from audience phones etc, and the presenter slides, if relevant.

Storage
--------------------

SD card? connect to laptop between talks?

Editing
--------------------

Mae sure audio and video is in sync. Include presentation slides where appropriate.

