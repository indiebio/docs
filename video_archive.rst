.. _archive:

Video archive
=============

You can find `our video archive here`__.
If you prefer, you can also watch our videos `on Peertube`__ or `on YouTube`__.

__ https://meetings-archive.debian.net/pub/debian-meetings/
__ https://peertube.debian.social/a/debconf/video-channels
__ https://www.youtube.com/channel/UC7SbfAPZf8SMvAxp8t51qtQ

We are using automated tools to generate metadata from our video archive and
then use this data to upload the videos to YouTube.

* Our `metadata and scraping tools`__.
* Our `YouTube tools`__, and `their documentation`__.

__ https://salsa.debian.org/debconf-video-team/archive-meta
__ https://salsa.debian.org/debconf-video-team/youtube
__ https://debconf-video-team.pages.debian.net/youtube/

Managing the archive
--------------------

The video archive lives on :code:`viardot.debian.org` and is managed with
:code:`git-annex`. In depth documentation on how to use :code:`git-annex` is
available `here`__.

__ https://git-annex.branchable.com/walkthrough/

Working on viardot directly
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. warning::
    If you decide to work on viardot directly, it is important to make the
    changes as the :code:`videoteam` user.

Once a video has gone through the entire review pipeline, it should be added to
viardot from :code:`vittoria.debian.org` (our current SReview machine)
automatically using the :code:`sreview-upload` script.

Once files are on viardot, you need to add them to the git-annex repository::

  sudo -u videoteam git annex add $path
  sudo -u videoteam git annex sync -m "Your commit message"

Working from a local copy
^^^^^^^^^^^^^^^^^^^^^^^^^

.. note::
    If possible, we recommend you work on viardot directly. Files created by
    pushing with your local user will not be owned by the :code:`videoteam`
    user by default and this will cause problems for other team members.

You'll need to have access to viardot to clone and sync the archive::

  git clone ssh://$YOURNICK@viardot.debian.org:/srv/video.debian.net/meetings-archive

You can also download the full archive, but beware, it's more than 1.5 TB of
data::

  git annex get

To add new data::

  git annex add $path
  git annex sync -m "Your commit message"

To edit data::

  git annex unlock $path
  # edit what needs editing
  git annex add $path
  git annex sync -m "Your commit message"

Archive mirrors
---------------

There are a few mirrors of our video archive:

#. https://meetings-archive.debian.net/pub/debian-meetings/: official mirror,
   updated hourly at :54. Contact :ref:`the debconf videoteam <contact us>`.

#. https://mirror.netcologne.de/debian-video/: updated 4 times per day. Contact
   Alexander Wirt.

#. http://mirror.ox.ac.uk/sites/meetings-archive.debian.net/pub/debian-meetings/:
   updated daily at 04:25 UK time. Contact sysdev@it.ox.ac.uk

If you want to run a new mirror, please :ref:`contact us <contact us>`! We will
be more than happy to provide you with a password-protected :code:`rsync` URI.
