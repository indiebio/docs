.. _working with videoteam:

Working with the Video team
===========================

If you plan to organise a conference — be it in-person or online — and intend
to record it, the first thing you should do is :ref:`contact us <contact us>`!
If we can, we'll be happy to help you.

To ensure the quality of the recorded videos and the streams and to make things
easier for everyone, this page describes our requirements and some guidelines.
We can be flexible, and if something listed here cannot be achieved, we may work
around it, but we would prefer not to.

General requirements
--------------------

#. You should :ref:`contact us <contact us>` **as soon as possible** to see if
   the dates you chose work for us. Please do not take for granted our
   participation, especially for in-person events.

#. You should have a good idea of the scale of your event in terms of number of
   talks, parallel tracks and potential attendees. A single-track weekend
   conference for 50 people and a two-week long conference with three parallel
   tracks for 300 people require very different resources.

#. As the organisers of a conference, we expect you to find volunteers to help
   with the video tasks. We'll gladly setup training sessions for them and
   teach everyone how the video systems work. Depending on the type of
   conference you are organising, you can consult our :ref:`in-person
   <volunteers>` or :ref:`online volunteer roles <online volunteers>` to get a
   better idea of how many people you will need.

In-person conferences
---------------------

#. In-person conferences require the physical presence of at least two core
   video team members, and ideally four. We encourage you to take in account the
   eventual travel and accommodation costs this entails when you create your
   budget.

#. Although in some cases we're able to provide a part of it ourselves, you will
   likely have to rent some :ref:`video equipment<hardware>`. Once you give us
   plans of the rooms you intend to use, we will provide you with a clear list
   of the equipment you will need to rent.

#. To avoid a number of problems, your venue will need to meet certain
   criteria. You can find a detailed checklist on our :ref:`venue checklist
   <Venue checklist>` page.

#. We encourage you to use `wafer`_, the conference website framework DebConf
   has been using since 2016. If need be, we will be happy to help you set up
   and configure an instance.

.. _wafer: https://github.com/CTPUG/wafer

Online conferences
------------------

#. We expect speakers to pre-record their talks in advance. Live talks at an
   online conference often lead to technical problems, resulting in bad quality
   videos. We strongly encourage you to refer speakers to our :ref:`advice for
   recording online presentations <advice for recording>`.

#. We strongly suggest you use `wafer`_, the conference website framework
   DebConf has been using since 2016. If need be, we will be happy to help you
   set up and configure an instance.
